#!/bin/sh
# Debuging tool for building installers.
build/bin/libtbx.python modules/cctbx_project/libtbx/auto_build/create_installer.py \
  --install_script  modules/xfel_regression/installer/xfel_installer.py \
  --version         dev \
  --binary \
  tmp/xfel-installer-dev
