from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
import os
from dials.command_line.dials_import import phil_scope # implicit import
from tst_iota_ransac import get_items, compare_with_ground_truth

class test_iota(object):
  def __init__(self):
    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)
    self.exafel_repo = libtbx.env.find_in_repositories(
        relative_path="exafel_project",
        test=os.path.isdir)
    self.LS49_NKS_repo = libtbx.env.find_in_repositories(
        relative_path="LS49",
        test=os.path.isdir)

  def test_iota_single_lattice(self):
    if self.xfel_regression is None:
      print ("Skipping IOTA single lattice test: xfel_regression not present")
      return
    if self.exafel_repo is None:
      print ("Skipping IOTA single lattice test: exafel_project not present")
      return

    single_lattice_dir = os.path.join(self.xfel_regression, "iota_test_data_and_scripts/LS49_sim/01_stills_process")
    stills_process_exec = os.path.join(self.exafel_repo, "ADSE13_25/command_line/stills_process_filter_spots.py")
    #stills_process_exec = os.path.join(self.exafel_repo, "ADSE13_25/stills_process_iota_baseline.py")
    phil_path = os.path.join(single_lattice_dir, "params.phil")

    # Create temp dir to do all the work there
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="single_lattice")
    os.chdir(tmp_dir)
    print ("Test results will be found at", tmp_dir)

    # Now run the test
    image_input = os.path.join(single_lattice_dir,'step5_MPIbatch_000000.img.gz')
    command = os.path.join("libtbx.python %s %s %s"%(stills_process_exec,phil_path,image_input))
    print (command)
    result = easy_run.fully_buffered(command=command, stdout_splitlines=True).raise_if_errors()

    # Now check the output
    # Only checking the A-matrix for now
    tol_1 = 0.05 # angle_tolerance
    tol_2 = 0.05 # uc_rel_length_tolerance
    from dxtbx.model.experiment_list import ExperimentListFactory
    exp = ExperimentListFactory.from_json_file('idx-step5_MPIbatch_000000.img_refined.expt', check_format=False)
    exp0 = ExperimentListFactory.from_json_file(os.path.join(single_lattice_dir,'ransac_reference_answer', 'idx-step5_MPIbatch_000000.img_refined_experiments.json'), check_format=False)

    crys = exp.crystals()
    crys0 = exp0.crystals()
    assert len(crys) == len(crys0) == 1, 'Number of indexed crystals should be 1 for single-lattice case'
    # Commenting out is_similar_to because no accounting for symmetry is done before comparing two crystal models
    #assert Crystal(crys[0]).is_similar_to(crys0[0], angle_tolerance=tol_1, uc_rel_length_tolerance=tol_2), 'crystal model is dissimilar to reference model provided'
    # Instead just make sure orientation matrix is similar for now 
    Z_score_tolerance=0.5
    from exafel_project.ADSE13_25.clustering.consensus_functions import get_dij_ori
    assert get_dij_ori(crys[0], crys0[0]) < Z_score_tolerance
    os.chdir(cwd)

    # Check with ground truth
    if self.LS49_NKS_repo is None:
      print ('Need LS49 repo to compare with ground truth')
    else:
      print ('Comparing w\ith ground truth now')
      # Declare some variables to compare with ground truth
      json_glob = [os.path.join(tmp_dir,"idx-step5_MPIbatch_000000.img_refined.expt")]
      image_glob = [os.path.join(single_lattice_dir,'step5_MPIbatch_000000.img.gz')]
      tag='idx-step5_MPIbatch_'
      compare_with_ground_truth(json_glob, image_glob, single_lattice_dir, tag)

  def test_iota_double_lattice(self):
    if self.xfel_regression is None:
      print ("Skipping IOTA double lattice test: xfel_regression not present")
      return
    if self.exafel_repo is None:
      print ("Skipping IOTA double lattice test: exafel_project not present")
      return

    double_lattice_dir = os.path.join(self.xfel_regression, "iota_test_data_and_scripts/LS49_sim/images_2bin")
    stills_process_exec = os.path.join(self.exafel_repo, "ADSE13_25/command_line/stills_process_filter_spots.py")
    #stills_process_exec = os.path.join(self.exafel_repo, "ADSE13_25/stills_process_iota_baseline.py")
    phil_path = os.path.join(double_lattice_dir, "params.phil")

    # Create temp dir to do all the work there
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="double_lattice")
    os.chdir(tmp_dir)
    print ("Test results will be found at", tmp_dir)

    # Now run the test
    image_input = os.path.join(double_lattice_dir,'composite_00000.cbf')
    command = os.path.join("libtbx.python %s %s %s"%(stills_process_exec,phil_path,image_input))
    print (command)
    exit()
    result = easy_run.fully_buffered(command=command, stdout_splitlines=True).raise_if_errors()

    # Now check whether the results are OK
    from dxtbx.model.experiment_list import ExperimentListFactory
    exp=ExperimentListFactory.from_json_file('idx-composite_00000_refined.expt', check_format=False)
    exp0=ExperimentListFactory.from_json_file(os.path.join(double_lattice_dir,'ransac_reference_answer', 'idx-composite_00000_refined_experiments.json'), check_format=False)
    crys = exp.crystals()
    crys0 = exp0.crystals()
    assert len(crys) == len(crys0) == 2, 'Number of indexed crystals should be 2 for double-lattice case'
    Z_score_tolerance=0.5
    from exafel_project.ADSE13_25.clustering.consensus_functions import get_dij_ori
    assert sum([get_dij_ori(crys[0],crys0[0])<Z_score_tolerance,
                get_dij_ori(crys[0],crys0[1])<Z_score_tolerance]) == 1, 'Cryst-0 should be similar to one of the reference models'

    assert sum([get_dij_ori(crys[1],crys0[0])<Z_score_tolerance,
                get_dij_ori(crys[1],crys0[1])<Z_score_tolerance]) == 1, 'Cryst-1 should be similar to one of the reference models'


    os.chdir(cwd)
    # Check with ground truth
    if self.LS49_NKS_repo is None:
      print ('Need LS49 repo to compare with ground truth')
    else:
      print ('Comparing w\ith ground truth now')
      # Declare some variables to compare with ground truth
      single_lattice_dir = os.path.join(self.xfel_regression, "iota_test_data_and_scripts/LS49_sim/01_stills_process")
      json_glob = [os.path.join(tmp_dir,"idx-composite_00000_refined.expt"),
                   os.path.join(tmp_dir,"idx-composite_00000_refined.expt")]
      image_glob = [os.path.join(single_lattice_dir,'step5_MPIbatch_000000.img.gz'),
                    os.path.join(single_lattice_dir,'step5_MPIbatch_000001.img.gz')]
      tag='idx-composite_'
      compare_with_ground_truth(json_glob, image_glob, single_lattice_dir, tag)

  def run_all(self):
    #self.test_iota_single_lattice()
    self.test_iota_double_lattice()

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()
  tester = test_iota()
  tester.run_all()
