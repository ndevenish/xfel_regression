from __future__ import division, print_function
import libtbx.load_env
from libtbx import easy_run
import os, glob
from dxtbx.model import Crystal
from dials.command_line.dials_import import phil_scope # implicit import
from dxtbx.format.Registry import Registry
from cctbx import crystal_orientation
from scitbx.matrix import sqr
import math


message = ''' Testing IOTA RANSAC on two possible cases (a) simulated one lattice (b) simulated two lattice
              Note that in both cases right now only the crystal model is being compared. No comparison is being
              being made regarded number of spots indexed or integrated due to this being WIP. Also the comparison
              being models are being done after casting it to dxtbx Crystal to avoid comparison being Crystal and Mosaic Crystal (bug in dials/dials#875)'''


global format_class

def get_items(json_glob,image_glob,tag):
  ''' Code taken from nksauter/LS49'''
  file_list = json_glob
  format_class = None
  for ii,item in enumerate(file_list):
    if tag=='idx-step5_MPIbatch_':
      serial_no=int(os.path.basename(item).split(tag)[1][0:6])
    if tag=='idx-composite_':
      serial_no=int(os.path.basename(item).split(tag)[1][0:5])
    #serial_no = int(item[-37:-32])
    #print (image_file)
    image_file = image_glob[ii]
    if format_class is None:
      format_class = Registry.find(image_file)
    i = format_class(image_file)
    Z = i.get_smv_header(image_file)
    ABC = Z[1]["DIRECT_SPACE_ABC"]
    abc = tuple([float(a) for a in ABC.split(",")])

    from dxtbx.model.experiment_list import ExperimentListFactory
    EC = ExperimentListFactory.from_json_file(item,check_format=False).crystals()

    yield(dict(serial_no=serial_no,ABC=abc,integrated_crystal_model=EC))


def compare_with_ground_truth(json_glob, image_glob, lattice_dir, tag, angular_tolerance=0.1):
  ''' Code taken from nksauter/LS49
      Compares indexing solution in json_glob with ground truth in image headers in image_glob'''
  pdb_lines = open(os.path.join(lattice_dir, "../PDB/1m2a.pdb"),"r").read()
  from LS49.sim.util_fmodel import gen_fmodel
  GF = gen_fmodel(resolution=3.0,pdb_text=pdb_lines,algorithm="fft",wavelength=1.7)
  CB_OP_C_P = GF.xray_structure.change_of_basis_op_to_primitive_setting() # from C to P
  print(str(CB_OP_C_P))

  icount=0
  from scitbx.array_family import flex
  angles=flex.double()
  for stuff in get_items(json_glob, image_glob, tag):
    #print stuff
    icount+=1
    print("Iteration",icount)
    # work up the crystal model from integration
    for crystal_model in stuff["integrated_crystal_model"]:
      from dxtbx.model import MosaicCrystalSauter2014
      mosaic_model=MosaicCrystalSauter2014(crystal_model)
      direct_A=mosaic_model.get_A_inverse_as_sqr()
      #direct_A = stuff["integrated_crystal_model"].get_A_inverse_as_sqr()
      permute = sqr((0,0,1,0,1,0,-1,0,0))
      sim_compatible = direct_A*permute # permute columns when post multiplying
      from cctbx import crystal_orientation
      integrated_Ori = crystal_orientation.crystal_orientation(sim_compatible, crystal_orientation.basis_type.direct)
      #integrated_Ori.show(legend="integrated")

      # work up the ground truth from header
      header_Ori = crystal_orientation.crystal_orientation(stuff["ABC"], crystal_orientation.basis_type.direct)
      #header_Ori.show(legend="header_Ori")

      C2_ground_truth = header_Ori.change_basis(CB_OP_C_P.inverse())
      C2_ground_truth.show(legend="C2_ground_truth")

      # align integrated model with ground truth
      try:
        cb_op_align = integrated_Ori.best_similarity_transformation(C2_ground_truth,50,1)
      except RuntimeError as e:
        continue
      aligned_Ori = integrated_Ori.change_basis(sqr(cb_op_align))
      aligned_Ori.show(legend="integrated, aligned")
      print("alignment matrix", cb_op_align)

      U_integrated = aligned_Ori.get_U_as_sqr()
      U_ground_truth = C2_ground_truth.get_U_as_sqr()

      missetting_rot = U_integrated * U_ground_truth.inverse()
      print("determinant",missetting_rot.determinant())
      from libtbx.test_utils import approx_equal
      assert approx_equal(missetting_rot.determinant(),1.0)
      angle,axis = missetting_rot.r3_rotation_matrix_as_unit_quaternion().unit_quaternion_as_axis_and_angle(deg=True)
      #angles.append(angle)
      #print "Item %5d angular offset is %8.5f deg."%(icount,angle)

      # now calculate the angle as mean a_to_a,b_to_b,c_to_c
      aoff = aligned_Ori.a.angle(C2_ground_truth.a,deg=True)
      boff = aligned_Ori.b.angle(C2_ground_truth.b,deg=True)
      coff = aligned_Ori.c.angle(C2_ground_truth.c,deg=True)
      # solved:  the reason missetting_rot doesn't exactly align postref and ground_truth is
      # that it's a monoclinic lattice, not orthorhombic.  Difference in the beta angle prevents exact alignment

      hyp = flex.mean(flex.double((aoff,boff,coff)))

      angles.append(hyp)
      print("Item %5d angular offset is %12.9f deg."%(icount,hyp))
      assert hyp < angular_tolerance, 'Deviation greater than angular tolerance'

  print("RMSD", math.sqrt(flex.mean(angles*angles)))

class test_iota(object):
  def __init__(self):
    self.xfel_regression = libtbx.env.find_in_repositories(
        relative_path="xfel_regression",
        test=os.path.isdir)
    self.exafel_repo = libtbx.env.find_in_repositories(
        relative_path="exafel_project",
        test=os.path.isdir)
    self.LS49_NKS_repo = libtbx.env.find_in_repositories(
        relative_path="LS49",
        test=os.path.isdir)

  def test_iota_single_lattice(self):
    if self.xfel_regression is None:
      print ("Skipping IOTA single lattice test: xfel_regression not present")
      return
    if self.exafel_repo is None:
      print ("Skipping IOTA single lattice test: exafel_project not present")
      return

    single_lattice_dir = os.path.join(self.xfel_regression, "iota_test_data_and_scripts/LS49_sim/01_stills_process")
    stills_process_exec = os.path.join(self.exafel_repo, "ADSE13_25/command_line/stills_process_filter_spots.py")
    phil_path = os.path.join(single_lattice_dir, "params_1.phil")

    # Create temp dir to do all the work there
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="single_lattice")
    os.chdir(tmp_dir)
    print ("Test results will be found at", tmp_dir)

    # Now run the test
    image_input = os.path.join(single_lattice_dir,'step5_MPIbatch_000000.img.gz')
    command = os.path.join("libtbx.python %s %s %s"%(stills_process_exec,phil_path,image_input))
    print (command)
    result = easy_run.fully_buffered(command=command, stdout_splitlines=True).raise_if_errors()
    # Now check the output
    # Only checking the A-matrix for now
    tol_1 = 0.05 # angle_tolerance
    tol_2 = 0.05 # uc_rel_length_tolerance
    from dxtbx.model.experiment_list import ExperimentListFactory
    exp = ExperimentListFactory.from_json_file('idx-step5_MPIbatch_000000.img_refined.expt', check_format=False)
    exp0 = ExperimentListFactory.from_json_file(os.path.join(single_lattice_dir,'ransac_reference_answer', 'idx-step5_MPIbatch_000000.img_refined_experiments.json'), check_format=False) 

    crys = exp.crystals()
    crys0 = exp0.crystals()
    assert len(crys) == len(crys0) == 1, 'Number of indexed crystals should be 1 for single-lattice case'
    # Commenting out is_similar_to because no accounting for symmetry is done before comparing two crystal models
    #assert Crystal(crys[0]).is_similar_to(crys0[0], angle_tolerance=tol_1, uc_rel_length_tolerance=tol_2), 'crystal model is dissimilar to reference model provided'
    # Instead just make sure orientation matrix is similar for now 
    Z_score_tolerance=0.5
    from exafel_project.ADSE13_25.clustering.consensus_functions import get_dij_ori
    assert get_dij_ori(crys[0], crys0[0]) < Z_score_tolerance
    os.chdir(cwd)

    # Check with ground truth
    if self.LS49_NKS_repo is None:
      print ('Need LS49 repo to compare with ground truth')
    else:
      print ('Comparing w\ith ground truth now')
      # Declare some variables to compare with ground truth
      json_glob = [os.path.join(tmp_dir,"idx-step5_MPIbatch_000000.img_refined.expt")]
      image_glob = [os.path.join(single_lattice_dir,'step5_MPIbatch_000000.img.gz')]
      tag='idx-step5_MPIbatch_'
      compare_with_ground_truth(json_glob, image_glob, single_lattice_dir, tag)

  def test_iota_double_lattice(self):
    if self.xfel_regression is None:
      print ("Skipping IOTA double lattice test: xfel_regression not present")
      return
    if self.exafel_repo is None:
      print ("Skipping IOTA double lattice test: exafel_project not present")
      return

    tol_1 = 0.05 # angle_tolerance
    tol_2 = 0.05 # uc_rel_length_tolerance
    double_lattice_dir = os.path.join(self.xfel_regression, "iota_test_data_and_scripts/LS49_sim/images_2bin")
    stills_process_exec = os.path.join(self.exafel_repo, "ADSE13_25/command_line/stills_process_filter_spots.py")
    phil_path = os.path.join(double_lattice_dir, "params_1.phil")

    # Create temp dir to do all the work there
    from libtbx.test_utils import open_tmp_directory
    cwd = os.path.abspath(os.curdir)
    tmp_dir = open_tmp_directory(suffix="double_lattice")
    os.chdir(tmp_dir)
    print ("Test results will be found at", tmp_dir)

    # Now run the test
    image_input = os.path.join(double_lattice_dir,'composite_00000.cbf')
    command = os.path.join("libtbx.python %s %s %s"%(stills_process_exec,phil_path,image_input))
    print (command)
    result = easy_run.fully_buffered(command=command, stdout_splitlines=True).raise_if_errors()
    # Now check whether the results are OK
    from dxtbx.model.experiment_list import ExperimentListFactory
    exp=ExperimentListFactory.from_json_file('idx-composite_00000_refined.expt', check_format=False)
    exp0=ExperimentListFactory.from_json_file(os.path.join(double_lattice_dir,'ransac_reference_answer', 'idx-composite_00000_refined_experiments.json'), check_format=False)
    crys = exp.crystals()
    crys0 = exp0.crystals()
    assert len(crys) == len(crys0) == 2, 'Number of indexed crystals should be 2 for double-lattice case'
    #assert sum([Crystal(crys[0]).is_similar_to(crys0[0],angle_tolerance=tol_1, uc_rel_length_tolerance=tol_2),
    #            Crystal(crys[0]).is_similar_to(crys0[1],angle_tolerance=tol_1, uc_rel_length_tolerance=tol_2)]) == 1, 'Cryst-0 should be similar to one of the reference models'
    #assert sum([Crystal(crys[1]).is_similar_to(crys0[0],angle_tolerance=tol_1, uc_rel_length_tolerance=tol_2),
    #            Crystal(crys[1]).is_similar_to(crys0[1],angle_tolerance=tol_1, uc_rel_length_tolerance=tol_2) ]) == 1, 'Cryst-1 should be similar to one of the reference models'
    Z_score_tolerance=0.5
    from exafel_project.ADSE13_25.clustering.consensus_functions import get_dij_ori
    assert sum([get_dij_ori(crys[0],crys0[0])<Z_score_tolerance,
                get_dij_ori(crys[0],crys0[1])<Z_score_tolerance]) == 1, 'Cryst-0 should be similar to one of the reference models'

    assert sum([get_dij_ori(crys[1],crys0[0])<Z_score_tolerance,
                get_dij_ori(crys[1],crys0[1])<Z_score_tolerance]) == 1, 'Cryst-1 should be similar to one of the reference models'

    os.chdir(cwd)
    # Check with ground truth
    if self.LS49_NKS_repo is None:
      print ('Need LS49 repo to compare with ground truth')
    else:
      print ('Comparing w\ith ground truth now')
      # Declare some variables to compare with ground truth
      single_lattice_dir = os.path.join(self.xfel_regression, "iota_test_data_and_scripts/LS49_sim/01_stills_process")
      json_glob = [os.path.join(tmp_dir,"idx-composite_00000_refined.expt"),
                   os.path.join(tmp_dir,"idx-composite_00000_refined.expt")]
      image_glob = [os.path.join(single_lattice_dir,'step5_MPIbatch_000000.img.gz'),
                    os.path.join(single_lattice_dir,'step5_MPIbatch_000001.img.gz')]
      tag='idx-composite_'
      compare_with_ground_truth(json_glob, image_glob, single_lattice_dir, tag)

  def run_all(self):
    self.test_iota_single_lattice()
    self.test_iota_double_lattice()

if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()
  tester = test_iota()
  tester.run_all()
