This test data is derived from the data used for the last panel of figure 3 in Brewster et. al. 2018, Acta Crystallogr D Struct Biol 74, 877-894.

Specifically, the cspad.cbf_metrology command was used to find the best results from indexing thermolysin images and used those images to refine the cspad in 'expanding' mode, keeping the detector flat.  Details here:
https://github.com/phyy-nx/dials_refinement_brewster2018/wiki/Metrology-refinement

The command used was:
cspad.cbf_metrology $scratch/results/r001*/000/out $scratch/results/r002[0-2]/000/out flat_refinement=True flat_refinement_with_distance=True method=expanding n_subset=3000 n_subset_method=n_refl n_refl_panel_list=10,26,42,58 | tee out.log

Instead, here we use a precombined set of crystals, the same inputs that would be found by cspad.cbf_metrology, but skipping the search of 100K images.

Note, the paper refined 3000 images.  Here, the both the first 300 images and the full dataset are provided.
