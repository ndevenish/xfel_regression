import sys, os
from libtbx.mpi4py import MPI, mpi_abort_on_exception
from xfel_regression.test.util import mpi_library

def run_test():
  import subprocess
  run_args = [
      ('decorated', 'raise', '3'),
      ('decorated', 'raise', '4'),
      ('decorated', 'exit', '3'),
      ('decorated', 'exit', '4'),
      ('undecorated', 'raise', '3'),
      ('undecorated', 'raise', '4'),
      ('undecorated', 'exit', '3'),
      ('undecorated', 'exit', '4'),
  ]

  for mode, action, nranks in run_args:

    command = ['mpirun', '-n', nranks]
    if mpi_library == 'openmpi':
      command.append('--oversubscribe')
    command.extend(['libtbx.python', __file__, mode, action])

    timed_out = False
    try:
      result = subprocess.run(command, capture_output=True, timeout=5)
    except subprocess.TimeoutExpired:
      timed_out = True

    stdout = result.stdout.decode()
    stderr = result.stderr.decode()

    if mode == 'undecorated' and nranks == '4':
      assert timed_out
    elif (mode, action, nranks) == ('decorated', 'raise', '4'):
      assert not timed_out
      assert 'ERRORMESSAGE' in stderr
      assert 'Traceback' in stderr
      assert 'MPI_Abort' in stderr
    elif (mode, action, nranks) == ('decorated', 'exit', '4'):
      assert not timed_out
      assert 'EXITMESSAGE' in stderr
      assert 'MPI_Abort' in stderr
    else:
      assert not timed_out
      for n in range(int(nranks)):
        assert f"ok from rank {n}" in stdout.split('\n')
      assert stdout.split('\n').count('done') == int(nranks)


def run_payload(mode, action):

  def one_rank_exit(rank):
    if rank == 3:
      sys.exit('EXITMESSAGE')
    else:
      print('ok from rank', rank)

  def one_rank_raise(rank):
    if rank == 3:
      raise RuntimeError('ERRORMESSAGE')
    else:
      print('ok from rank', rank)

  assert action in ['exit', 'raise']
  if action == 'exit': func = one_rank_exit
  elif action == 'raise': func = one_rank_raise

  assert mode in ['decorated', 'undecorated']
  if mode == 'decorated': func = mpi_abort_on_exception(func)

  func(MPI.COMM_WORLD.rank)
  MPI.COMM_WORLD.barrier()
  print('done')

if __name__ == '__main__':
  if len(sys.argv) == 1:
    run_test()
  else:
    assert len(sys.argv) == 3
    mode, action = sys.argv[1:]
    run_payload(mode, action)
