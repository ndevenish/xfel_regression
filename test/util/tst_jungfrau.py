from __future__ import division, print_function
import numpy as np
from xfel.util import jungfrau


def test_panel_correction():
  # assume flat-field of 1 photon on detector
  # 1x1 pixels measure value 1
  # 1x2 pixels measure value 2
  # 2x2 pixels measure value 4

  # make the Jungfrau panel, with double and quadruple pixels
  img = np.ones((512, 1024)).astype(np.float64)
  for row in 255, 256:
    img[row]*=2
  for col in 255,256,511,512,767,768:
    img[:,col]*=2

  unique_vals = list(np.unique(img))
  assert len(unique_vals)==3
  assert 1 in unique_vals  # normal sized pixels
  assert 2 in unique_vals  # double sized pixels (2x1)
  assert 4 in unique_vals  # quadrouple sized pixels (2x2)

  # after correction, all pixels should be 1x1 and have signal = 1
  img2 = jungfrau.correct_panel(img)
  assert img2.shape == (514, 1030)
  assert np.all(img2==1)
  print("OK")


if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()
  test_panel_correction()

