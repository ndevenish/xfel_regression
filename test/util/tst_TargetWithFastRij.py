from xfel.merging.application.modify.aux_cosym import TargetWithFastRij
import libtbx, os, pickle

class test_rij_wij_computer:
  def __init__(self):
    self.xfel_regression_dir = libtbx.env.find_in_repositories(
      relative_path="xfel_regression",
      test=os.path.isdir)
    self.ref_data_path = os.path.join(
        self.xfel_regression_dir,
        "merging_test_data",
        "cosym_input_data",
        "rij_wij_refdata.pickle"
    )
    self.test_data_path = os.path.join(
        self.xfel_regression_dir,
        "merging_test_data",
        "cosym_input_data",
        "rij_wij_testdata.pickle"
    )

  def run(self):
    with open(self.ref_data_path, 'rb') as f:
      rij_ref = pickle.load(f)
      wij_ref = pickle.load(f)

    ref_instance = TargetWithFastRij(test_data_path=self.test_data_path)
    rij_dials, wij_dials = ref_instance._compute_rij_wij(use_super=True)

    test_instance = TargetWithFastRij(test_data_path=self.test_data_path)
    rij_test, wij_test = test_instance._compute_rij_wij()

    assert rij_test.size == wij_test.size
    assert rij_test.size == rij_ref.size
    assert rij_test.size == rij_dials.size
    for i in range(len(rij_test)):
      for ii in range(rij_test[i].size):
        assert libtbx.test_utils.approx_equal(
            rij_test.item((i,ii)), rij_ref.item((i,ii))
        )
        assert libtbx.test_utils.approx_equal(
            wij_test.item((i,ii)), wij_ref.item((i,ii))
        )
        assert libtbx.test_utils.approx_equal(
            rij_test.item((i,ii)), rij_dials.item((i,ii))
        )
        assert libtbx.test_utils.approx_equal(
            wij_test.item((i,ii)), wij_dials.item((i,ii))
        )

if __name__=="__main__":
  test_rij_wij_computer().run()
