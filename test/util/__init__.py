from __future__ import division, print_function

def mpi_library():
  # Try to determine which MPI library is available
  from libtbx import easy_run
  command = "mpirun --version"
  result = easy_run.fully_buffered(command).raise_if_errors()
  output = "\n".join(result.stdout_lines).lower()
  if "open mpi" in output:
    return "openmpi"
  if "mpich" in output:
    return "mpich"
  else:
    raise RuntimeError('Unrecognized MPI library. Stdout:\n'+"\n".join(result.stdout_lines))
