from __future__ import division
from dials_data import download
from libtbx.easy_run import fully_buffered
from libtbx.test_utils import open_tmp_directory
from dxtbx.model.experiment_list import ExperimentList
from dials.array_family import flex
import os

def tst_small_cell_process():
  data_root = download.DataFetcher()("4fluoro_cxi", pathlib=True)
  data_root /= "lcls_2022_smSFX_workshop_data"

  tmp_dir = open_tmp_directory(suffix="tst_small_cell_process")
  os.chdir(tmp_dir)
  print ("Assertion results will be found at", tmp_dir)

  src_phil_path = data_root / "indexing" / "params_1.phil"
  phil_path = "params_1.phil"
  with open(phil_path, 'w') as f:
    for line in open(src_phil_path):
      if "reference_geometry" in line:
        f.write("  reference_geometry = " + str(data_root / "calibration" / "r0215.expt\n"))
      elif "mask" in line:
        f.write("    mask = " + str(data_root / "calibration" / "psana_border_hot_shift4.mask\n"))
      else:
        f.write(line)

  command = "cctbx.xfel.small_cell_process params_1.phil " + str(data_root / "ten_cbfs" / "*.cbf")
  fully_buffered(command).raise_if_errors()

  expts = ExperimentList.from_file("idx-0000_refined.expt", check_format=False)
  assert len(expts) == 10, len(expts)

  refls = flex.reflection_table.from_file('idx-0000_indexed.refl')
  assert len(refls) in list(range(42,48)), len(refls)

  print("OK")

if __name__ == "__main__":
  tst_small_cell_process()
