from __future__ import division
import os
from xfel.small_cell.powder_util import Spotfinder_radial_average, Center_scan
from dxtbx.model.experiment_list import ExperimentList
from dials.array_family import flex
from xfel.small_cell.command_line.powder_from_spots import phil_scope as program_phil_scope
from iotbx.phil import parse
from libtbx.test_utils import open_tmp_directory, approx_equal
from libtbx.utils import show_times_at_exit

test_phil_scope_template = """
center_scan {
  d_min = 4.85
  d_max = 4.98
}
plot.interactive = False
output {
  plot_file = %s
  xy_file = %s
}
"""

class test_powder_plotting:
  def __init__(self):

    # Create an ouput directory and memorize it
    tmp_dir = open_tmp_directory(suffix="_powder_from_spots")
    self.output_dir = os.path.abspath(os.path.join(tmp_dir, "out"))
    os.mkdir(self.output_dir)


    # Setup output paths and complete the phil
    self.out_plot_path = os.path.join(self.output_dir, 'plot.png')
    self.out_xy_path = os.path.join(self.output_dir, 'pattern.xy')
    test_phil_scope = parse(
        test_phil_scope_template % (self.out_plot_path, self.out_xy_path)
    )
    self.params = program_phil_scope.fetch(test_phil_scope).extract()


  def run(self):
    from dials_data import download
    test_data_root = download.DataFetcher()('4fluoro_cxi', pathlib=True)
    test_data_root = test_data_root.joinpath(
        'lcls_2022_smSFX_workshop_data',
        'spotfinding'
    )
    self.test_expt_path = test_data_root.joinpath('r0165_strong.expt')
    self.test_refl_path = test_data_root.joinpath('r0165_strong.refl')

    experiments = ExperimentList.from_file(self.test_expt_path, check_format=False)
    reflections = flex.reflection_table.from_file(self.test_refl_path)

    det_origin_start = experiments[0].detector.hierarchy().get_local_origin()

    cscan = Center_scan(experiments, reflections, self.params)
    starts, ends, net_shifts = [], [], []
    for step in self.params.center_scan.step_px:
      start, end, net_shift = cscan.search_step(step)
      starts.append(start)
      ends.append(end)
      net_shifts.append(net_shift)
    for start, end in zip(starts, ends):
      assert end <= start
    assert sorted(starts, reverse=True) == starts
    assert sorted(ends, reverse=True) == ends
    final_shift = net_shifts[-1]
    assert approx_equal(final_shift, [-0.0421875, -0.00234375, 0])
    det_origin_end = experiments[0].detector.hierarchy().get_local_origin()
    for start, shift, end in zip(det_origin_start, final_shift, det_origin_end):
      assert approx_equal(start + shift, end)

    averager = Spotfinder_radial_average(experiments, reflections, self.params)
    averager.calculate()
    averager.plot()

    xy_result = []
    with open(self.out_xy_path) as f:
      for l in f:
        x, y = l.split()
        xy_result.append((float(x), float(y)))
    xy_max = max(xy_result, key=lambda x: x[1])
    assert len(xy_result) == 3000
    assert xy_max == (2.310656, 140.0)
    assert xy_result[0] == (20.,0.)
    assert xy_result[-1] == (2.,0.)
    assert os.path.isfile(self.out_plot_path)

    print('ok')


if __name__=="__main__":
  show_times_at_exit()
  tester = test_powder_plotting()
  tester.run()
