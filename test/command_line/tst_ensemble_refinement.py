from __future__ import division, print_function
import os, time
from dials.array_family import flex
from dials.tests.command_line.test_stills_process import sacla_phil
from libtbx import easy_run
from libtbx.test_utils import approx_equal, open_tmp_directory
from xfel.command_line.cxi_mpi_submit import Script as submit_script
from xfel.ui.components.submission_tracker import QueueInterrogator
from dials_data.download import DataFetcher

class test_ensemble_refinement(object):
  def __init__(self):
    df = DataFetcher()
    self.image_examples = df('image_examples', pathlib=True)

  def test_ensemble_refinement(self):
    """
    This test exercises the GUI workflow up through ensemble refinement by running these steps:
    - cxi.mpi_submit: process the SACLA 4 image dataset using local mode job submission (os.fork)
    - track job using the GUI's QueueInterrogator
    - cctbx.xfel.stripe_experiment: ensemble refinement of the results, again using local mode job submission
    - cctbx.xfel.detector_residulas: check refinement. RMSD shoudl fall from ~80 to ~40 microns.
    Finally it checks the number of reintegrated reflections
    """

    if not os.path.exists(self.image_examples):
      print ("Skipping test_ensemble_refinement: data not found (%s)"%(self.image_examples))
      return

    image_path = os.path.join(self.image_examples, "SACLA-MPCCD-run266702-0-subset.h5")
    assert os.path.isfile(image_path), image_path

    geometry_path = os.path.join(self.image_examples,
                                 "SACLA-MPCCD-run266702-0-subset-refined_experiments_level1.json")
    assert os.path.isfile(geometry_path)

    tmp_dir = open_tmp_directory(suffix="tst_ensemble_refinement")
    os.chdir(tmp_dir)

    # Write the .phil configuration to a file
    with open("process_sacla.phil", "w") as f:
      f.write(sacla_phil% geometry_path)

    args = """
      mp.method=local
      mp.nproc=3
      {image_path}
      input.run_num=run266702-0
      input.trial=0
      input.rungroup=1
      input.dispatcher=dials.stills_process
      mp.use_mpi=False
      input.target=process_sacla.phil
    """.format(image_path=image_path).split()
    pid = submit_script().run(args)

    tracker = QueueInterrogator('local')

    timer = 0
    while True:
      status = tracker.query(pid)
      if status == "DONE":
        break
      else:
        assert timer < 20, "Processing timeout"
        print("Waiting for processing to finish:", status)
        time.sleep(1)
        timer += 1

    cwd = os.getcwd()
    taskdir = os.path.join(cwd, 'run266702-0', '000_rg001', 'task001')
    os.mkdir(taskdir)

    args = " ".join("""
      combine_experiments.clustering.use=False
      striping.results_dir={cwd}
      striping.run=run266702-0
      striping.trial=0
      striping.rungroup=1
      mp.method=local
      mp.local.include_mp_in_command=False
      mp.use_mpi=False
      mp.mpi_command=None
      striping.output_folder={taskdir}
      combine_experiments.reference_from_experiment.average_detector=True
      combine_experiments.reference_from_experiment.detector=None
    """.format(cwd=cwd, taskdir=taskdir).split())

    command = "cctbx.xfel.stripe_experiment %s"%args
    print (command)
    result = easy_run.fully_buffered(command=command).raise_if_errors()
    result.show_stdout()

    intermediates = os.path.join(taskdir, "combine_experiments_t000", "intermediates")
    refined =  os.path.join(intermediates, "t000_rg001_chunk000_refined.*")
    command = "cctbx.xfel.detector_residuals hierarchy=1 show_plots=False %s"%refined
    print (command)
    result = easy_run.fully_buffered(command=command).raise_if_errors()
    result.show_stdout()

    rmsd = float(result.stdout_lines[-8].split()[-1]) # should be around 41.4 microns

    assert approx_equal(rmsd, 41, eps = 1e-0, out=None), rmsd

    reintegrated = os.path.join(intermediates, "t000_rg001_chunk000_reintegrated.refl")
    refls = flex.reflection_table.from_file(reintegrated)
    assert len(refls) in list(range(580, 595)), len(refls)

    print ("OK")

  def run_all(self):
    self.test_ensemble_refinement()


if __name__ == '__main__':
  from libtbx.utils import show_times_at_exit
  show_times_at_exit()

  tester = test_ensemble_refinement()
  tester.run_all()
