#!/usr/bin/env bash
temp=$1
if [[ $temp == "all" ]];then
  step_list="None";
else step_list=`echo $temp | tr "," " "`
fi
echo "STEP LIST:"$step_list

input_path=$2
input_experiments_suffix=$3
input_reflections_suffix=$4
scaling_model=$5
statistics_model=$6
postrefine=$7
postrefine_algorithm=$8
error_model=$9
merge_anomalous=${10}
set_average_unit_cell=${11}
save_experiments_and_reflections=${12}
output_dir=${13}
output_prefix=${14}
cores=${15}
mtz_column_F=${16}
filter_unit_cell=${17}
cosym_sg=${18}
mpi_extra=${19}
scaling_algorithm=${20}
scaling_unitcell=${21}
scaling_spacegroup=${22}
echo "NUMBER OF CORES: "${cores}

effective_params="\
input.path=${input_path} \
input.experiments_suffix=${input_experiments_suffix} \
input.reflections_suffix=${input_reflections_suffix} \
input.parallel_file_load.method=uniform \
filter.algorithm=unit_cell \
filter.unit_cell.value.target_unit_cell=${filter_unit_cell} \
filter.unit_cell.value.relative_length_tolerance=0.02 \
filter.outlier.min_corr=-1.0 \
select.algorithm=significance_filter \
select.significance_filter.sigma=0.5 \
scaling.algorithm=${scaling_algorithm} \
scaling.model=${scaling_model} \
scaling.mtz.mtz_column_F=${mtz_column_F} \
scaling.resolution_scalar=0.95 \
scaling.pdb.solvent_algorithm=flat \
scaling.unit_cell=${scaling_unitcell}
scaling.space_group=${scaling_spacegroup}
postrefinement.enable=${postrefine} \
postrefinement.algorithm=${postrefine_algorithm} \
merging.d_min=2.0 \
merging.merge_anomalous=${merge_anomalous} \
merging.set_average_unit_cell=${set_average_unit_cell} \
merging.error.model=${error_model} \
statistics.n_bins=10 \
statistics.cciso.mtz_file=${statistics_model} \
statistics.cciso.mtz_column_F=${mtz_column_F} \
output.output_dir=${output_dir} \
output.prefix=${output_prefix} \
output.do_timing=True \
output.log_level=1 \
output.save_experiments_and_reflections=${save_experiments_and_reflections} \
parallel.a2a=1 \
modify.cosym.anchor=True
modify.cosym.min_pairs=3 \
modify.cosym.d_min=4 \
modify.cosym.min_reflections=15 \
modify.cosym.normalisation=None \
modify.cosym.dimensions=2 \
modify.cosym.space_group=${cosym_sg} \
modify.cosym.use_curvatures=False \
modify.reindex_to_reference.dataframe=test_reindex_dataframe.pickle \
modify.cosym.dataframe=test_cosym_dataframe.pickle \
"

mkdir -p $output_dir

if [[ ${cores} > 1 ]];then
  mpiexec -n ${cores} ${mpi_extra} \
    cctbx.xfel.merge dispatch.step_list="${step_list}" ${effective_params};
else
  cctbx.xfel.merge dispatch.step_list="${step_list}" ${effective_params};
fi
